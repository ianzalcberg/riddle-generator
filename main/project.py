import random
def main():
    QuestionChoice = random.randint(1, 3)
    if QuestionChoice == 1:
        Question = "The more there is of me, the less you see."
        CorrectAnswer = "darkness"
    elif QuestionChoice == 2:
        Question = "I have hands but i cannot clap."
        CorrectAnswer = "a clock"
    else:
        Question = "I have a neck, but no head."
        CorrectAnswer = "a bottle"
    # this chooses what question
    
    Response = input(Question + " What am I?")
    # this asks the question

    if CorrectAnswer == Response:
        print(Response + " is the correct answer, congratulations!")
    else:
        print(Response + " is incorrect, the correct response was " + CorrectAnswer + ".")
    #this gives feedback to the user on the question they just solved



if __name__ == '__main__':
    main()
